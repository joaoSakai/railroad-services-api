package com.joaosakai.railroadservicesapi.api;

import com.joaosakai.railroadservicesapi.exceptions.RouteInfoException;
import com.joaosakai.railroadservicesapi.services.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author joaoSakai
 */
@RestController
public class RoutesInfoAPI {

    private RouteService routeService;

    @Autowired
    public RoutesInfoAPI(RouteService routeService) {
        this.routeService = routeService;
    }

    @GetMapping(path = "/calculate/{route}")
    public Integer calculateRoute(@PathVariable String route) throws RouteInfoException {
        return routeService.calculateRouteDistance(route);
    }

    @GetMapping(path = "/calculate/shortest/{route}")
    public Integer calculateShortestRoute(@PathVariable String route) throws RouteInfoException {
        return routeService.calculateShortestRoute(route);
    }

    @GetMapping(path = "/count/route/e/{route}")
    public Integer calculateOccurrencesExactlyStops(@PathVariable String route) throws RouteInfoException {
        return routeService.calculateRouteOccurrenceEquals(route);
    }

    @GetMapping(path = "/count/route/le/{route}")
    public Integer calculateOccurrencesLessOrEqualStops(@PathVariable String route) throws RouteInfoException {
        return routeService.calculateRouteOccurrenceLessOrEqual(route);
    }

    @GetMapping(path = "/calculate/diff/{limit}")
    public Integer calculateDifferentRoutes(@PathVariable Integer limit) throws RouteInfoException {
        return routeService.calculateCDifferentRoutes(limit);
    }





}
