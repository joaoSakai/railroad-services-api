package com.joaosakai.railroadservicesapi.engines;

import org.springframework.stereotype.Component;

@Component
public class ShortestSearchWithStops {

    /**
     * Method to count the route between the source and the destination in the graph matrix
     * @param graph Graph matrix
     * @param source Source Town index
     * @param destination Destination Town index
     * @param maxStops Number of max Routes Stops
     * @param totalVertices Fixed number of Vertices;
     * @return Number of occurrences
     */
    public int countRoutes(int graph[][], int source, int destination, int maxStops, final int totalVertices) {
        if (maxStops == 0 && source == destination) {
            return 1;
        }
        if (maxStops == 1 && graph[source][destination] == 1) {
            return 1;
        }
        if (maxStops <= 0) {
            return 0;
        }

        int occurrences = 0;

        /**
         * Navigate into all inner routes from source
         */
        for (int i = 0; i < totalVertices; i++) {
            if (graph[source][i] == 1)  {
                occurrences += countRoutes(graph, i, destination, maxStops-1, totalVertices);
            }

        }

        return occurrences;
    }
}
