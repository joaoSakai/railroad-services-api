package com.joaosakai.railroadservicesapi.engines;

import com.joaosakai.railroadservicesapi.models.Route;
import com.joaosakai.railroadservicesapi.models.Town;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Class used to calculate the shortest search between to vertexes. the Dijkstra algorithm was used;
 * @see <a href="https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm"> https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm <a/>
 *
 * @author joaoSakai
 */
@Component
public class ShortestSearch {

    private List<Town> vertexes;
    private List<Route> edges;

    private Set<Town> settledNodes;
    private Set<Town> unSettledNodes;
    private Map<Town, Town> predecessors;
    private Map<Town, Integer> distance;

    public void execute(Town source) {
        settledNodes = new HashSet<>();
        unSettledNodes = new HashSet<>();
        distance = new HashMap<>();
        predecessors = new HashMap<>();
        distance.put(source, 0);
        unSettledNodes.add(source);

        while (unSettledNodes.size() > 0) {
            Town node = getMinimum(unSettledNodes);
            settledNodes.add(node);
            unSettledNodes.remove(node);
            findMinimalDistances(node);
        }
    }

    private void findMinimalDistances(Town node) {
        List<Town> adjacentNodes = getNeighbors(node);
        for (Town target : adjacentNodes) {
            if (getShortestDistance(target) > getShortestDistance(node) + getDistance(node, target)) {
                distance.put(target, getShortestDistance(node) + getDistance(node, target));
                predecessors.put(target, node);
                unSettledNodes.add(target);
            }
        }
    }

    private int getDistance(Town node, Town target) {
        for (Route edge : edges) {
            if (edge.getSource().equals(node)
                    && edge.getDestination().equals(target)) {
                return edge.getDistance();
            }
        }
        throw new RuntimeException();
    }

    private List<Town> getNeighbors(Town node) {
        List<Town> neighbors = new ArrayList<Town>();
        for (Route edge : edges) {
            if (edge.getSource().equals(node) && !isSettled(edge.getDestination())) {
                neighbors.add(edge.getDestination());
            }
        }
        return neighbors;
    }

    private Town getMinimum(Set<Town> vertexes) {
        Town minimum = null;
        for (Town vertex : vertexes) {
            if (minimum == null) {
                minimum = vertex;
            } else {
                if (getShortestDistance(vertex) < getShortestDistance(minimum)) {
                    minimum = vertex;
                }
            }
        }
        return minimum;
    }

    private boolean isSettled(Town vertex) {
        return settledNodes.contains(vertex);
    }

    private int getShortestDistance(Town destination) {
        Integer d = distance.get(destination);
        if (d == null) {
            return Integer.MAX_VALUE;
        } else {
            return d;
        }
    }

    /**
     * This method returns the path from the source to the destination and NULL if no path exists
     * @param target Town to be used as destiny
     * @return List of Path
     */
    public LinkedList<Town> getPath(final Town target) {
        LinkedList<Town> path = new LinkedList<>();
        Town step = target;
        // check if a path exists
        if (predecessors.get(step) == null) {
            return null;
        }
        path.add(step);
        while (predecessors.get(step) != null) {
            step = predecessors.get(step);
            path.add(step);
        }
        // Put it into the correct order
        Collections.reverse(path);
        return path;
    }

    public void setVertexes(List<Town> vertexes) {
        this.vertexes = vertexes;
    }

    public void setEdges(List<Route> edges) {
        this.edges = edges;
    }
}
