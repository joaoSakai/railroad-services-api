package com.joaosakai.railroadservicesapi.parser;

import com.joaosakai.railroadservicesapi.exceptions.RouteInfoException;
import com.joaosakai.railroadservicesapi.models.SearchRoute;
import com.joaosakai.railroadservicesapi.models.SimpleRoute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class responsible for parsing the concatenated route
 *
 * @author joaoSakai
 */
@Component
public class RouteParser {


    private final Logger logger = LoggerFactory.getLogger(RouteParser.class);

    /**
     * Parse a route request
     * @param route RouteEntity String to be parsed (i.e) ABC, CEBC
     * @return List of parsed routes
     */
    public List<SimpleRoute> parseSimpleRoute(final String route) {

        if (isInvalidRoute(route)) {
            return Collections.emptyList();
        }

        final List<SimpleRoute> simpleRoutes = new ArrayList<>();
        int next = 1;
        for (int i = 0; i < route.length(); i++) {

            if(next == route.length()) {
                break;
            }

            final String town = String.valueOf(route.charAt(i));
            final String destinationTown = String.valueOf(route.charAt(next));

            simpleRoutes.add(new SimpleRoute(town, destinationTown));

            next++;
        }
        return simpleRoutes;

    }

    /**
     * Parse string route containing the source, destination and maxStops info
     * @param route Route request i.e AC4
     * @return SearchRoute object with source, destination and maxStops;
     * @throws RouteInfoException
     */
    public SearchRoute parseCountRoute(final String route) throws RouteInfoException {
        if(route.length() < 3) {
            throw new RouteInfoException("Invalid route: " + route);
        }

        String source       = getStringFromCharIndex(route, 0);
        String destination  = getStringFromCharIndex(route, 1);
        Integer maxStops    = Integer.valueOf(getStringFromCharIndex(route, 2));

        return new SearchRoute(source, destination, maxStops);
    }

    /**
     *
     * @param route
     * @return
     */
    public SimpleRoute parseFastestRoute(String route) throws RouteInfoException {
        if(route.length() > 2) {
            throw new RouteInfoException("Invalid route: " + route);
        }

        return new SimpleRoute(
                getStringFromCharIndex(route, 0),
                getStringFromCharIndex(route, 1)
        );
    }

     /**
     * Validate route string parameter
     * @param route RouteEntity to be verified
     * @return True if the route is invalid
     */
    private boolean isInvalidRoute(String route) {
        if (route == null || StringUtils.isEmpty(route)) {
            logger.error("There is no route to be parsed");
            return true;
        }

        if (!hasDestination(route)) {
            logger.error("There is no destination in the route: %s", route);
            return true;
        }
        return false;
    }

    /**
     * Verify if route has destination town
     *
     * @param route RouteEntity to be verified
     * @return true if the route has a destination town
     */
    private boolean hasDestination(final String route) {
        return route.length() > 0;
    }

    private String getStringFromCharIndex(final String route, final Integer index) {
        return String.valueOf(route.charAt(index));
    }

}
