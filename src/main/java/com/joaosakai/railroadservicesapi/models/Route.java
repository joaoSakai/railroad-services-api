package com.joaosakai.railroadservicesapi.models;

public class Route {

    private Town source;
    private Town destination;
    private Integer distance;

    public Route(Town source, Town destination, Integer distance) {
        this.source = source;
        this.destination = destination;
        this.distance = distance;
    }

    public Route() {}

    public Town getSource() {
        return source;
    }

    public Town getDestination() {
        return destination;
    }

    public Integer getDistance() {
        return distance;
    }
}
