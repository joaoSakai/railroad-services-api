package com.joaosakai.railroadservicesapi.models;

public class Town {

    private String name;

    public Town(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
