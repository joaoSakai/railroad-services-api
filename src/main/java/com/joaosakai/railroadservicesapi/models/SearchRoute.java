package com.joaosakai.railroadservicesapi.models;

public class SearchRoute {

    private String source;
    private String destination;
    private Integer maxStops;

    public SearchRoute(String source, String destination, Integer maxStops) {
        this.source = source;
        this.destination = destination;
        this.maxStops = maxStops;
    }

    public String getSource() {
        return source;
    }

    public String getDestination() {
        return destination;
    }

    public Integer getMaxStops() {
        return maxStops;
    }
}
