package com.joaosakai.railroadservicesapi.models;

public class SimpleRoute {

    private String source;
    private String destination;

    public SimpleRoute(String source, String destination) {
        this.source = source;
        this.destination = destination;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }
}
