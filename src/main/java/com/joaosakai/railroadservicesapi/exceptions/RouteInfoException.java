package com.joaosakai.railroadservicesapi.exceptions;

public class RouteInfoException extends Exception {

    public RouteInfoException() {}

    public RouteInfoException(String message) {
        super(message);
    }
}
