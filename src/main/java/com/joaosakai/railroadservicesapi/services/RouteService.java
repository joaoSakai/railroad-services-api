package com.joaosakai.railroadservicesapi.services;

import com.joaosakai.railroadservicesapi.engines.ShortestSearch;
import com.joaosakai.railroadservicesapi.engines.ShortestSearchWithStops;
import com.joaosakai.railroadservicesapi.exceptions.RouteInfoException;
import com.joaosakai.railroadservicesapi.models.Route;
import com.joaosakai.railroadservicesapi.graph.RouteGraph;
import com.joaosakai.railroadservicesapi.models.SearchRoute;
import com.joaosakai.railroadservicesapi.models.SimpleRoute;
import com.joaosakai.railroadservicesapi.models.Town;
import com.joaosakai.railroadservicesapi.parser.RouteParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Service responsible to all requested operations based on the Routes;
 *
 * @author joaoSakai
 */
@Service
public class RouteService {

    private static final String NO_SUCH_ROUTE_MESSAGE = "NO SUCH ROUTE";

    @Autowired
    private RouteParser routeParser;

    @Autowired
    private ShortestSearchWithStops shortestSearchWithStops;

    @Autowired
    private ShortestSearch shortestSearch;

    /**
     * Calculate distance according to the routes
     * @param routeRequest List of routes in to be calculated;
     * @return Information about the total route distance;
     * @throws Exception
     */
    public Integer calculateRouteDistance(final String routeRequest) throws RouteInfoException {

        final List<SimpleRoute> simpleRoutes = routeParser.parseSimpleRoute(routeRequest);

        int distance = 0;
        for(SimpleRoute route : simpleRoutes) {

            RouteGraph routeGraph = RouteGraph.getInstance();

            Route foundedRoute = routeGraph.findSourceDestination(route.getSource(), route.getDestination());

            if(foundedRoute == null) {
                throw new RouteInfoException(NO_SUCH_ROUTE_MESSAGE);
            }
            distance += foundedRoute.getDistance();
        }
        return distance;
    }

    /**
     * Calculate the occurrences in the specific route with a max Stops
     * @param route Route request object i.e AC4 where A is a source town, C is a destination town and 4 is the maxStops count;
     * @return The number of occurrences that matches the path with the maxStops condition
     * @throws RouteInfoException
     */
    public Integer calculateRouteOccurrenceEquals(final String route) throws RouteInfoException {
        SearchRoute searchRoute = routeParser.parseCountRoute(route);

        RouteGraph routeGraph = RouteGraph.getInstance();

        int graphMatrix[][] = routeGraph.getGraphMatrix();

        final Integer sourceIndex = routeGraph.getVertexesIndex(searchRoute.getSource());
        final Integer destinationIndex = routeGraph.getVertexesIndex(searchRoute.getDestination());

        return shortestSearchWithStops.countRoutes(graphMatrix, sourceIndex, destinationIndex, searchRoute.getMaxStops(), routeGraph.getVertexesSize());
    }

    /**
     * Calculate the occurrences in the specific route with a max Stops
     * @param route Route request object i.e AC4 where A is a source town, C is a destination town and 4 is the maxStops count;
     * @return The number of occurrences that matches the path with the maxStops condition
     * @param route
     * @return
     */
    public Integer calculateRouteOccurrenceLessOrEqual(final String route) throws RouteInfoException {
        SearchRoute searchRoute = routeParser.parseCountRoute(route);

        RouteGraph routeGraph = RouteGraph.getInstance();

        int graphMatrix[][] = routeGraph.getGraphMatrix();

        final Integer sourceIndex = routeGraph.getVertexesIndex(searchRoute.getSource());
        final Integer destinationIndex = routeGraph.getVertexesIndex(searchRoute.getDestination());

        return shortestSearchWithStops.countRoutes(graphMatrix, sourceIndex, destinationIndex, searchRoute.getMaxStops() + 1, routeGraph.getVertexesSize());
    }

    /**
     * Calculate the shortest route between two towns
     * @param route Route request object i.e AC where A is a source town and C is a destination town
     * @return The shortest distance between the source and destination
     * @throws RouteInfoException
     */
    public Integer calculateShortestRoute(String route) throws RouteInfoException {
        SimpleRoute simpleRoute = routeParser.parseFastestRoute(route);

        RouteGraph routeGraph = RouteGraph.getInstance();

        shortestSearch.setVertexes(routeGraph.getVertexes());
        shortestSearch.setEdges(routeGraph.getEdges());

        shortestSearch.execute(routeGraph.getTown(simpleRoute.getSource()));

        final LinkedList<Town> path = shortestSearch.getPath(routeGraph.getTown(simpleRoute.getDestination()));

        return calculateRouteDistance(generateRouteString(path));
    }


    public Integer calculateCDifferentRoutes(final Integer limit) throws RouteInfoException {
        List<String> cRoutes = Arrays.asList("CDC", "CEBC", "CEBCDC", "CDCEBC", "CDEBC", "CEBCEBC", "CEBCEBCEBC");

        int count = 0;
        for(String route : cRoutes) {
            final List<SimpleRoute> simpleRoutes = routeParser.parseSimpleRoute(route);

            int distance = 0;
            for(SimpleRoute currentRoute : simpleRoutes) {

                RouteGraph routeGraph = RouteGraph.getInstance();

                Route foundedRoute = routeGraph.findSourceDestination(currentRoute.getSource(), currentRoute.getDestination());

                if(foundedRoute == null) {
                    throw new RouteInfoException(NO_SUCH_ROUTE_MESSAGE);
                }
                distance += foundedRoute.getDistance();
            }

            if(distance < limit) {
                count++;
            }

        }
        return count;
    }

    /**
     * Auxiliar method to generate a route string based on a towns list
     * @param path list of towns
     * @return route in a String format
     */
    private String generateRouteString(final List<Town> path) {
        String route = "";
        for(Town town : path) {
            route += town.getName();
        }
        return route;
    }

}
