package com.joaosakai.railroadservicesapi.graph;

import com.joaosakai.railroadservicesapi.models.Route;
import com.joaosakai.railroadservicesapi.models.Town;

import java.util.List;

/**
 * Singleton RouteGraph class representing the RouthGraph repository
 *
 * @author joaoSakai
 */
public class RouteGraph {

    private static RouteGraph instance = null;

    private List<Town> vertexes;
    private List<Route> edges;

    protected RouteGraph() {}

    public static RouteGraph getInstance() {
        if (instance == null) {
            instance = new RouteGraph();
        }
        return instance;
    }

    public List<Town> getVertexes() {
        return vertexes;
    }

    public List<Route> getEdges() {
        return edges;
    }

    public int[][] getGraphMatrix() {
        final int vertexesSize = vertexes.size();

        int graphMatrix[][] = new int[vertexesSize][vertexesSize];

        for(int i = 0; i < vertexes.size(); i++) {
            for(int j = 0; j < vertexes.size(); j++) {
                if(hasRoute(vertexes.get(i), vertexes.get(j))){
                    graphMatrix[i][j] = 1;
                } else {
                    graphMatrix[i][j] = 0;
                }
            }
        }

        return graphMatrix;
    }

    public Integer getVertexesIndex(final String source) {
        int sourceIndex = 0;
        for (int i = 0; i < vertexes.size(); i++) {
            if(vertexes.get(i).getName().equals(source)){
                sourceIndex = i;
            }
        }
        return sourceIndex;
    }

    public Integer getVertexesSize() {
        return vertexes.size();
    }

    private boolean hasRoute(Town a, Town b) {
        boolean found = false;
        for(Route route : edges) {
            if(route.getSource().getName().equals(a.getName())
                    && route.getDestination().getName().equals(b.getName())){
                found = true;
            }
        }
        return found;
    }

    public void setVertexes(List<Town> vertexes) {
        this.vertexes = vertexes;
    }

    public void setEdges(List<Route> edges) {
        this.edges = edges;
    }

    public Route findSourceDestination(String source, String destination) {
        Route foundedRoute = null;
        for(Route route : edges) {
            if(route.getSource().getName().equals(source) && route.getDestination().getName().equals(destination)) {
                foundedRoute = route;
                break;
            }
        }
        return foundedRoute;
    }

    public Town getTown(final String source) {
        Town foundedTown = null;
        for (Town town : vertexes) {
            if(source.equals(town.getName())) {
                foundedTown = town;
                break;
            }
        }
        return foundedTown;
    }
}
