package com.joaosakai.railroadservicesapi.api;

import com.joaosakai.railroadservicesapi.services.RouteService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RoutesDistanceInfoAPITest {

    private static final String CALCULATE_PATH = "/calculate/{route}";

    private MockMvc mockMvc;
    private RouteService routeService;

    @Before
    public void setUp() {
        routeService = mock(RouteService.class);

        mockMvc = standaloneSetup(new RoutesInfoAPI(routeService)).build();
    }

    @Test
    public void testRoute() throws Exception {
        final String route = "AB";

        mockMvc.perform(get(CALCULATE_PATH, route))
                .andExpect(status().isOk());

    }


}
