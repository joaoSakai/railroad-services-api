package com.joaosakai.railroadservicesapi.engines;

import com.joaosakai.railroadservicesapi.models.Route;
import com.joaosakai.railroadservicesapi.graph.RouteGraph;
import com.joaosakai.railroadservicesapi.models.Town;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ShortestSearchTest {

    @Autowired
    private ShortestSearch shortestSearch;

    @Test
    public void calculateShortestRoute() {

        List<Town> vertexes = new ArrayList<>();

        Town A = new Town("A");
        Town B = new Town("B");
        Town C = new Town("C");
        Town D = new Town("D");
        Town E = new Town("E");

        vertexes.add(A);
        vertexes.add(B);
        vertexes.add(C);
        vertexes.add(D);
        vertexes.add(E);

        List<Route> edges = new ArrayList<>();


        Route AB = new Route(A, B, 5);
        Route AD = new Route(A, D, 5);
        Route AE = new Route(A, E, 7);

        Route BC = new Route(B, C, 4);

        Route CD = new Route(C, D, 8);
        Route CE = new Route(C, E, 2);

        Route DC = new Route(D, C, 8);
        Route DE = new Route(D, E, 6);

        Route EB = new Route(E, B, 3);

        edges.add(AB);
        edges.add(AD);
        edges.add(AE);
        edges.add(BC);
        edges.add(CD);
        edges.add(CE);
        edges.add(DC);
        edges.add(DE);
        edges.add(EB);

        RouteGraph routeGraph = RouteGraph.getInstance();

        routeGraph.setEdges(edges);
        routeGraph.setVertexes(vertexes);

        shortestSearch.setEdges(routeGraph.getEdges());
        shortestSearch.setVertexes(routeGraph.getVertexes());

        shortestSearch.execute(A);

        LinkedList<Town> path = shortestSearch.getPath(C);

        Assert.assertNotNull(path);
    }
}
