package com.joaosakai.railroadservicesapi.engines;

import com.joaosakai.railroadservicesapi.models.Route;
import com.joaosakai.railroadservicesapi.models.Town;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShortestSearchWithStopsTest {

    static final int INF = Integer.MAX_VALUE;

    private List<Route> edges;
    private List<Town> vertexes;

    @Before
    public void setUp() {
        edges = new ArrayList<>();
        vertexes = new ArrayList<>();

        Town A = new Town("A");
        Town B = new Town("B");
        Town C = new Town("C");
        Town D = new Town("D");
        Town E = new Town("E");

        vertexes.add(A);
        vertexes.add(B);
        vertexes.add(C);
        vertexes.add(D);
        vertexes.add(E);

        Route AB = new Route(A, B, 5);
        Route AD = new Route(A, D, 5);
        Route AE = new Route(A, E, 7);

        Route BC = new Route(B, C, 4);

        Route CD = new Route(C, D, 8);
        Route CE = new Route(C, E, 2);

        Route DC = new Route(D, C, 8);
        Route DE = new Route(D, E, 6);

        Route EB = new Route(E, B, 3);

        edges.add(AB);
        edges.add(AD);
        edges.add(AE);
        edges.add(BC);
        edges.add(CD);
        edges.add(CE);
        edges.add(DC);
        edges.add(DE);
        edges.add(EB);
    }


    @Test
    public void testShortestPath() {

        final int vertexesSize = vertexes.size();

        int graphMatrix[][] = new int[vertexesSize][vertexesSize];

        for(int i = 0; i < vertexes.size(); i++) {
            for(int j = 0; j < vertexes.size(); j++) {
                if(hasRoute(vertexes.get(i), vertexes.get(j))){
                    graphMatrix[i][j] = 1;
                } else {
                    graphMatrix[i][j] = 0;
                }
            }
        }

        String source = "C";
        String destination = "C";
        Integer maxStops = 4;

        for (Town towm : vertexes) {
            if(towm.getName().equals(source)) {

            }
        }

        int sourceIndex = 0;
        int destinationIndex = 0;

        sourceIndex = getVertexesIndex(vertexesSize, source);
        destinationIndex = getVertexesIndex(vertexesSize, destination);

        ShortestSearchWithStops shortestSearchWithStops = new ShortestSearchWithStops();

        shortestSearchWithStops.countRoutes(graphMatrix, sourceIndex, destinationIndex, maxStops, vertexesSize);

        System.out.println("");
    }

    private Integer getVertexesIndex(int vertexesSize, String source) {
        int sourceIndex = 0;
        for (int i = 0; i < vertexesSize; i++) {
            if(vertexes.get(i).getName().equals(source)){
                sourceIndex = i;
            }
        }
        return sourceIndex;
    }

    private boolean hasRoute(Town a, Town b) {
        boolean found = false;
        for(Route route : edges) {
            if(route.getSource().getName().equals(a.getName())
                    && route.getDestination().getName().equals(b.getName())){
                found = true;
            }
        }
        return found;
    }
}
