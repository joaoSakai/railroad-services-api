package com.joaosakai.railroadservicesapi.parser;

import com.joaosakai.railroadservicesapi.models.SimpleRoute;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RouteParserTest {

    @Autowired RouteParser routeParser;

    @Test
    public void testInvalidRoute() {
        final String invalidRoute = "";

        final List<SimpleRoute> parsed = routeParser.parseSimpleRoute(invalidRoute);
        Assert.assertTrue(parsed.isEmpty());
    }

    @Test
    public void testOnlyDestinationRoute() {
        final String onlyDestinationRoute = "A";

        final List<SimpleRoute> parsed = routeParser.parseSimpleRoute(onlyDestinationRoute);
        Assert.assertTrue(parsed.isEmpty());
    }

    @Test
    public void testOneRouteParsed() {
        final String oneRoute = "AB";

        final List<SimpleRoute> parsed = routeParser.parseSimpleRoute(oneRoute);
        Assert.assertFalse(parsed.isEmpty());
        Assert.assertEquals(1, parsed.size());
    }

    @Test
    public void testMultiRoutesParsed() {
        final String multiRoute = "ABCD";

        final List<SimpleRoute> parsed = routeParser.parseSimpleRoute(multiRoute);

        Assert.assertFalse(parsed.isEmpty());
        Assert.assertEquals(3, parsed.size());

        Assert.assertEquals("AB", getRoutePair(parsed, 0));
        Assert.assertEquals("BC", getRoutePair(parsed, 1));
        Assert.assertEquals("CD", getRoutePair(parsed, 2));
    }


    private String getRoutePair(final List<SimpleRoute> parsed, final int index) {
        return parsed.get(index).getSource()+parsed.get(index).getDestination();
    }
}
