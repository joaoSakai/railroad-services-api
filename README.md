# railroad-services-api

Objective: Provide the routes information to the customers;
Back-end:  Spring boot 2.0 with Web starter / Java 8 

**Build & Run**
To test the project run: 	*mvn clean test*
To build the project run:	*mvn clean install*
To run the project:			*mvn spring-boot:run*

Host: localhost
Default port: 8080

Endpoints: 

**GET**		
/calculate/{route}
"Given a route return the distance information"
Use this endpoint to test the exercises from 1 to 5
{route} = ABC, AD, ADC, AEBCD, AED 

**GET**
/calculate/shortest/{route}
"Given a route return the shortest distance information"
Use this endpoint to test the exercise 8 
{route} = AC

**GET**
/count/route/e/{route}
"Given a route and maxStops information return the occurrences equals to the maxStops"
Use this endpoint to test the exercise 7 
{route} = AC4

**GET**
/count/route/le/{route}
"Given a route and maxStops information return the occurrences less or equals then maxStops"
Use this endpoint to test the exercise 6  
{route} = CC3

**GET**
"Given C routes return trips with less than 30 of distance"
/calculate/diff/{limit}
Use this endpoint to test the exercise 10
{limit} = 30



## Considerations
For the exercise 9: TODO implement a search method calculating the shortest path from X to X  
